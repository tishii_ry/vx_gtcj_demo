#include <NVX/nvx.h>
#include <NVXIO/FrameSource.hpp>
#include <NVXIO/Render.hpp>
#include <NVXIO/Application.hpp>

#include <iostream>
#include <string>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/contrib/contrib.hpp>

void keyEvent( void* event, vx_char key, vx_uint32 x, vx_uint32 y )
{
	bool* app_exit = static_cast<bool*>( event );
	if (key == 'q')
		*app_exit = true;
}
int main( int argc, char** argv )
{
	nvxio::Application &app = nvxio::Application::get();
	app.init( argc, argv );
	vx_context context = vxCreateContext();
	vx_graph graph = vxCreateGraph( context );

	// open video file
	std::string file = "signs.avi";
	std::unique_ptr< nvxio::FrameSource >
		 frame_source( nvxio::createDefaultFrameSource( context, file ) );
	frame_source->open();

	// create window
	nvxio::FrameSource::Parameters frame_config = frame_source->getConfiguration();
	int width = frame_config.frameWidth;
	int height = frame_config.frameHeight;
	std::unique_ptr< nvxio::Render >
		 render( nvxio::createDefaultRender( context, "graphmode", width, height ) );

	// register keybord event callback
	bool app_exit = false;
	render->setOnKeyboardEventCallback( keyEvent, &app_exit );

	// create vx images to use vx premitives
	vx_image frame = vxCreateImage( context, width, height, VX_DF_IMAGE_RGBX );
	vx_image gray = vxCreateVirtualImage( graph, 0, 0, VX_DF_IMAGE_U8 );
	vx_image blur = vxCreateVirtualImage( graph, 0, 0, VX_DF_IMAGE_U8 );
	vx_image edge = vxCreateVirtualImage( graph, 0, 0, VX_DF_IMAGE_U8 );
	vx_image view = vxCreateImage( context, width, height, VX_DF_IMAGE_U8 );

	// prepare threshold object for canny edge detector
	vx_threshold canny_thresh = vxCreateThreshold( context, VX_THRESHOLD_TYPE_RANGE, VX_TYPE_INT32 );
	int canny_lower = 230;  int canny_upper = 250;
	vxSetThresholdAttribute( canny_thresh, VX_THRESHOLD_ATTRIBUTE_THRESHOLD_LOWER,
							&canny_lower, sizeof(int) );
	vxSetThresholdAttribute( canny_thresh, VX_THRESHOLD_ATTRIBUTE_THRESHOLD_UPPER,
							&canny_upper, sizeof(int) );

	vx_node cnv_node = vxColorConvertNode( graph, frame, gray );
	vx_node blur_node = vxBox3x3Node( graph, gray, blur );
	vx_node edge_node = vxCannyEdgeDetectorNode( graph, blur, canny_thresh, 3, VX_NORM_L1, view );
	vxVerifyGraph( graph );

	// prepare timer
	cv::TickMeter tmeter;

	//
	// main loop
	//
	while (!app_exit)
	{

		tmeter.start();

		// update frame
		nvxio::FrameSource::FrameStatus frame_status = frame_source->fetch( frame );
		if (frame_status == nvxio::FrameSource::TIMEOUT)
			continue;
		if (frame_status == nvxio::FrameSource::CLOSED)
		{
			frame_source->open();
			continue;
		}

		// proc premitives
		vxProcessGraph( graph );

		// show result image
		render->putImage( view );
		render->flush();

		// get elapsed time
		tmeter.stop();
		std::cout << tmeter.getTimeMilli() << " ms" << std::endl;

		// reset timer
		tmeter.reset();
	}

	return 0;
}
