// add user-defined CUDA kernel
//#define _CUDA_PROC

#include <NVX/nvx.h>
#include <NVXIO/Application.hpp>
#include <NVXCUIO/FrameSource.hpp>
#include <NVXCUIO/Render.hpp>

#include <iostream>
#include <string>

#include <opencv2/contrib/contrib.hpp>

#define _CUDA_PROC
#ifdef _CUDA_PROC
__global__ void kernel( void* input, void* output,  uint32_t w, uint32_t h, uint32_t pitch )
{
	uint32_t gidx = blockDim.x * blockIdx.x + threadIdx.x;
	uint32_t gidy = blockDim.y * blockIdx.y + threadIdx.y;

	if ((gidx < w) && (gidy < h))
	{
		uint32_t pos = gidx + pitch * gidy;
		uint8_t v = ((uint8_t*)input)[pos];
		((uint8_t*)output)[pos] = (uint8_t)(v == 0 ? 255 : 0);
	}
}
#endif

nvxcu_pitch_linear_image_t createImage( uint32_t width, uint32_t height, nvxcu_df_image_e type );
void releaseImage(nvxcu_pitch_linear_image_t * image);
void keyEvent( void* event, vx_char key, vx_uint32 x, vx_uint32 y );

int main( int argc, char** argv )
{
	nvxio::Application &app = nvxio::Application::get();
	app.init( argc, argv );

	// open video source
	std::string file = "signs.avi";
//	std::string file = "device:///v4l2?index=0";

	std::unique_ptr< nvxcuio::FrameSource >
		 frame_source( nvxcuio::createDefaultFrameSource( file ) );
	frame_source->open();

	// create window
	nvxcuio::FrameSource::Parameters frame_config = frame_source->getConfiguration();
	int width = frame_config.frameWidth;
	int height = frame_config.frameHeight;
	std::unique_ptr< nvxcuio::Render >
		 render( nvxcuio::createDefaultRender( "nvxcu", width, height ) );

	// register keybord event callback
	bool app_exit = false;
	render->setOnKeyboardEventCallback( keyEvent, &app_exit );

	// memory allocation and prepare nvxcu objects
	nvxcu_pitch_linear_image_t frame = createImage( width, height, NVXCU_DF_IMAGE_RGBX );
	nvxcu_pitch_linear_image_t gray  = createImage( width, height, NVXCU_DF_IMAGE_U8 );
	nvxcu_pitch_linear_image_t blur  = createImage( width, height, NVXCU_DF_IMAGE_U8 );
	nvxcu_pitch_linear_image_t equal = createImage( width, height, NVXCU_DF_IMAGE_U8 );
	nvxcu_pitch_linear_image_t edge  = createImage( width, height, NVXCU_DF_IMAGE_U8 );
	nvxcu_pitch_linear_image_t view  = createImage( width, height, NVXCU_DF_IMAGE_U8 );

	// prepare exec target for nvxcu
	int32_t dev_id = -1;
	cudaGetDevice( &dev_id );
	nvxcu_stream_exec_target_t exec_target = {};
	exec_target.base.exec_target_type = NVXCU_STREAM_EXEC_TARGET;
	exec_target.stream = nullptr;
	cudaGetDeviceProperties( &exec_target.dev_prop, dev_id );

	// prepare box filter border setting
	nvxcu_border_t border = {};
	nvxcu_pixel_value_t border_val = {};
	border.constant_value = border_val;
	border.mode = NVXCU_BORDER_MODE_REPLICATE;

	// prepare canny edge prop
	nvxcu_tmp_buf_t tmp_buf;
	nvxcu_tmp_buf_size_t canny_buf_size, equal_buf_size, tmp_buf_size;
	canny_buf_size = nvxcuCannyEdgeDetector_GetBufSize( width, height,
							NVXCU_DF_IMAGE_U8, 3, &exec_target.dev_prop );
	equal_buf_size = nvxcuEqualizeHist_GetBufSize( width, height,
							NVXCU_DF_IMAGE_U8, &exec_target.dev_prop );
	tmp_buf_size.dev_buf_size = (canny_buf_size.dev_buf_size > equal_buf_size.dev_buf_size) ?
					(canny_buf_size.dev_buf_size) : (equal_buf_size.dev_buf_size);
	tmp_buf_size.host_buf_size = (canny_buf_size.host_buf_size > equal_buf_size.host_buf_size) ?
					(canny_buf_size.host_buf_size) : (equal_buf_size.host_buf_size);
	if (tmp_buf_size.dev_buf_size > 0)
		cudaMalloc( &tmp_buf.dev_ptr, tmp_buf_size.dev_buf_size );
	if (tmp_buf_size.host_buf_size > 0)
		cudaMallocHost( &tmp_buf.host_ptr, tmp_buf_size.host_buf_size );

	// prepare timer
	cv::TickMeter tmeter;

	//
	// main loop
	//
	while (!app_exit)
	{

		tmeter.start();

		// update frame
		nvxcuio::FrameSource::FrameStatus frame_status = frame_source->fetch( frame );
		if (frame_status == nvxcuio::FrameSource::TIMEOUT)
			continue;
		if (frame_status == nvxcuio::FrameSource::CLOSED)
		{
			frame_source->open();
			continue;
		}

		// proc premitives
		nvxcuColorConvert( &frame.base, &gray.base, NVXCU_COLOR_SPACE_DEFAULT,
							NVXCU_CHANNEL_RANGE_FULL, &exec_target.base );
		nvxcuMedian3x3( &gray.base, &blur.base, &border, &exec_target.base );
		nvxcuEqualizeHist( &blur.base, &equal.base, &tmp_buf, &exec_target.base );
#ifndef _CUDA_PROC
		nvxcuCannyEdgeDetector( &equal.base, 230, 250, (int8_t)255, (int8_t)0, 3,
					NVXCU_NORM_L1, &view.base, &tmp_buf, &exec_target.base );

#else
		nvxcuCannyEdgeDetector( &equal.base, 230, 250, (int8_t)255, (int8_t)0, 3,
					NVXCU_NORM_L1, &edge.base, &tmp_buf, &exec_target.base );
		dim3 blockDim(64,2);
		dim3 gridDim((width+blockDim.x-1)/blockDim.x, (height+blockDim.y-1)/blockDim.y);
		kernel<<<gridDim, blockDim, 0, exec_target.stream>>>(edge.planes[0].dev_ptr,
			 view.planes[0].dev_ptr, width, height, edge.planes[0].pitch_in_bytes);
#endif
		cudaStreamSynchronize(exec_target.stream);

		// show result image
		render->putImage( view );
		render->flush();

		// get elapsed time
		tmeter.stop();
		std::cout << tmeter.getTimeMilli() << " ms" << std::endl;

		// reset timer
		tmeter.reset();
	}

	releaseImage( &frame );
	releaseImage( &gray );
	releaseImage( &blur );
	releaseImage( &equal );
	releaseImage( &edge );
	releaseImage( &view );
	return 0;
}

nvxcu_pitch_linear_image_t createImage( uint32_t width, uint32_t height, nvxcu_df_image_e type )
{
	void* dev_ptr = NULL;
	size_t pitch = 0;
	uint32_t sizeof_type = (type == NVXCU_DF_IMAGE_U8) ? (sizeof(uint8_t) * 1)
                                                           : (sizeof(uint8_t) * 4);
	cudaMallocPitch( &dev_ptr, &pitch, width * sizeof_type, height );

	nvxcu_pitch_linear_image_t image;
	image.base.image_type = NVXCU_PITCH_LINEAR_IMAGE;
	image.base.format = type;
	image.base.width = width;
	image.base.height = height;
	image.planes[0].dev_ptr = dev_ptr;
	image.planes[0].pitch_in_bytes = pitch;

	return image;
}
void releaseImage(nvxcu_pitch_linear_image_t * image)
{
    cudaFree(image->planes[0].dev_ptr);
}

void keyEvent( void* event, vx_char key, vx_uint32 x, vx_uint32 y )
{
	bool* app_exit = static_cast<bool*>( event );
	if (key == 'q')
		*app_exit = true;
}

