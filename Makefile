PROG1 := vx_gtcj_demo_cv
CPP1  := main_gtcj2016.cpp
OBJ1  := $(CPP1:.cpp=.o)
PROG2 := vx_gtcj_demo_graph
CPP2  := main_gtcj2016_graphmode.cpp
OBJ2  := $(CPP2:.cpp=.o)
PROG3 := vx_gtcj_demo_nvxcu
CPP3  := main_gtcj2016_nvxcu.cu
OBJ3  := $(CPP3:.cu=.o)

CXX  := g++
NVCC := nvcc

CXXFLAGS  := -std=c++0x
NVCCFLAGS := -std=c++11 -gencode arch=compute_53,code=sm_53 -gencode arch=compute_50,code=sm_50 -gencode arch=compute_52,code=sm_52
CCFLAGS := -O3 -DNDEBUG

# check visionworks availability
VISION_WORKS_EXISTS := $(shell pkg-config --exists visionworks && echo "1" || echo "0")
ifeq ($(VISION_WORKS_EXISTS), 0)
$(error You must put directory containing visionworks.pc to the PKG_CONFIG_PATH environment variable)
endif

# check opencv availability
OPENCV_EXISTS := $(shell pkg-config --exists opencv && echo "1" || echo "0")
ifeq ($(OPENCV_EXISTS), 0)
$(error You must put directory containing opencv.pc to the PKG_CONFIG_PATH environment variable)
endif

# check visionworks availability
NPP_EXISTS := $(shell pkg-config --exists nppi-8.0 nppc-8.0 && echo "1" || echo "0")
ifeq ($(NPP_EXISTS), 0)
$(error You must put directory containing nppi-8.0.pc and nppc-8.0 to the PKG_CONFIG_PATH environment variable)
endif

INCLUDES := $(shell pkg-config --cflags nppi-8.0 nppc-8.0 visionworks nvxio opencv)
LIBRARIES := $(shell pkg-config --libs nppi-8.0 nppc-8.0 visionworks nvxio opencv)

LDFLAGS := -Wl,--allow-shlib-undefined -pthread
NVCC_LDFLAGS := -lpthread

####

all: $(PROG1) $(PROG2) $(PROG3)

%.o: %.cpp
	$(CXX) $(INCLUDES) $(CCFLAGS) $(CXXFLAGS) -o $@ -c $<

%.o: %.cu
	$(NVCC) $(INCLUDES) $(NVCCFLAGS) -o $@ -c $<

$(PROG1): $(OBJ1)
	$(CXX) $(LDFLAGS) -o $@ $^ $(LIBRARIES)

$(PROG2): $(OBJ2)
	$(CXX) $(LDFLAGS) -o $@ $^ $(LIBRARIES)

$(PROG3): $(OBJ3)
	$(NVCC) $(NVCC_LDFLAGS) $(NVCCFLAGS) -o $@ $^ $(LIBRARIES)

clean: 
	rm -f $(OBJ1) $(PROG1) $(OBJ2) $(PROG2) $(OBJ3) $(PROG3)



