#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/contrib/contrib.hpp>

int main()
{

	// open video source
	cv::VideoCapture cap( "signs.avi" );


	// create window
	cv::namedWindow( "opencv" );

	cv::Mat frame, gray, blur, equal, view;

	// prepare timer
	cv::TickMeter tmeter;

	//
	// main loop
	//
	while (1)
	{

		tmeter.start();

		// update frame
		cap >> frame;

		// proc premitives
		cv::cvtColor( frame, gray, CV_BGR2GRAY );
		cv::medianBlur( gray, blur, 3 );
		cv::equalizeHist( blur, equal );
		cv::Canny( blur, view, 230., 250., 3, false );

		// show result image
		cv::imshow( "capcamera", view );

		// get elapsed time
		tmeter.stop();
		std::cout << tmeter.getTimeMilli() << " ms" << std::endl;

		// exit when press 'q'
		if (cv::waitKey(30) == 'q')  break;

		// reset timer
		tmeter.reset();
	}

	return 0;
}
