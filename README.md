# README #

Demo application for Jetson TX1 devkit at session 2016-1060 in GTC Japan 2016.

> 「Jetson TX1開発キットで始めるCUDAプログラミング」

> Session ID: 2016-1060

> https://www.gputechconf.jp/sessions.html


### Tested Environment ###
- JetPack 2.3
- Linux For Tegra R24.2
- OpenCV4Tegra 2.4.13
- VisionWorks 1.5
- CUDA 8.0 Toolkit for L4T r24.2
### Set up ###
Install [JetPack 2.3](https://developer.nvidia.com/embedded/jetpack)
[(Installation guide)](http://docs.nvidia.com/jetpack-l4t/index.html#developertools/mobile/jetpack/l4t/2.3/jetpack_l4t_install.htm)

Install git
~~~~
sudo apt install git
~~~~
Clone repository
~~~~
git clone https://bitbucket.org/tishii_ry/vx_gtcj_demo
~~~~
Build
~~~~
cd vx_gtcj_demo
make
~~~~
Copy video file from visionworks directory
~~~~
cp /usr/share/visionworks/sources/data/signs.avi ./
~~~~

### How to run application ###

demo program with OpenCV
~~~~
./vx_gtcj_demo_cv
~~~~
demo program with VisionWorks NVXCU
~~~~
./vx_gtcj_demo_nvxcu
~~~~
demo program with VisionWorks Graphmode
~~~~
./vx_gtcj_demo_graph
~~~~